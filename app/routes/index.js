import Night from "appkit/models/night";
import Dream from "appkit/models/dream";
export default Ember.Route.extend({
	beforeModel: function() {
		this.transitionTo("night", Night.today());
	}

});