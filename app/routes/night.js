import Night from "appkit/models/night";
var NightRoute = Ember.Route.extend({
	model: function(params) {
		return Night.query({formatted: params.formatted});
	}
});

export default NightRoute;
