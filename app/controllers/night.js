var NightController = Ember.ObjectController.extend({

	archetypes: [
			{ dt: "Anxiety",	name: "Anxiety" },
			{ dt: "Nightmare",	name: "Nightmare" },
			{ dt: "Fantasy",	name: "Fantasy" },
			{ dt: "Lucid",		name: "Lucid" },
	],

	selectedDreamType: null,

	actions: {
		saveDream: function() {
			var newDream = this.get("newDream");
			var newType = this.get("selectedDreamType");

			if (Ember.isEmpty(newDream) || Ember.isEmpty(newDream.trim())) {
				return;
			}

      var model = this.get("model");

      model.addDream({ type: newType, content: newDream });

      this.send("resetForm");
		},

    resetForm: function() {
      this.set("newDream", "");
      this.set("selectedDreamType", null);
    }
  }
});

export default NightController;
