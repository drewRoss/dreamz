var DreamSpaceComponent = Ember.Component.extend({
  dreams: function() {
    return this.get("night.dreams");
  }.property("night"),

  actions: {
    removeDream: function(dream) {
      this.get("night").removeDream(dream);
    }
  }
});

export default DreamSpaceComponent;
