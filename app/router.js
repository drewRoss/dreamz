var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

Router.map(function() {
	this.route("index", { path: "/"} );	// We'll catch / and redirect user to current night
	this.resource("night", { path: "/:formatted" });	// Catch /#2014-03-03
  this.route('component-test');
  this.route('helper-test');
  // this.resource('posts', function() {
  //   this.route('new');
  // });
});

export default Router;
