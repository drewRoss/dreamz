import Dream from "appkit/models/dream";

var Night = Ember.Object.extend({

	// Returns instance of moment for the given timestamp
	moment: function() {
		console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~moment");
		return window.moment(this.get("timestamp"));
	}.property("timestamp"),

	// Return formatted date
	formatted: function() {
		console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~formatted");
		return this.get("moment").format("YYYY-MM-DD");
	}.property("moment"),

	// We'll use this as a key for the storage
	storageKey: function() {
		console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~storageKey");
		// When loading from localStorage I would use something like JSON.parse
		// to load a JSON array of serialized Dream objects for that Night.
		var fDream = "%@|%@".fmt(Night.namespace, this.get("formatted"));
		return fDream;
	}.property("formatted"),

  // Sleep & Dream
	dreams: function() {
		var dreams = window.localStorage[this.get("storageKey")];
    if(!dreams) return [];

		return JSON.parse(dreams);
	}.property("storageKey"),


	// Return next day
	tomorrow: function() {
		console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tomorrow");
		var tommorow = this.get("moment").clone().add(1, "day");
		if(window.moment().isAfter(tommorow)) {
			return Night.create({ timestamp: tommorow.valueOf() });
		}
	}.property("moment"),

	// Return previous day
	yesterday: function() {
		console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~yesterday");
		return Night.create({ timestamp: this.get("moment").clone().subtract(1, "day").valueOf() });
	}.property("moment"),

  addDream: function(attributes) {
    var dream = Dream.create(attributes);

    this.get("dreams").pushObject(dream);
    this.save();
  },

  removeDream: function(dream) {
    this.get("dreams").removeObject(dream);
    this.save();
  },

  serialize: function() {
    return JSON.stringify(this.get("dreams"));
  },

	// Write value into localStorage
	save: function() {

		// When saving: I would use JSON.stringify to save that serialized
		// array into the localStorage for the given Night
		// var saveDream = JSON.stringify(this.get(dreamObj));

		console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~save");
		window.localStorage[this.get("storageKey")] = this.serialize(); // saveDream;
	}
});

Night.reopenClass({
	namespace: "dreamz-json", // Namespace in the localStorage

	// Query by timestamp
	query: function(conditions) {
		console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~query");
		var timestamp = window.moment(conditions.formatted).startOf("day").valueOf();
		return Night.create({ timestamp: timestamp });
	},

	today: function() {
		console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~today");
		return Night.query({ formatted: window.moment() });
	}
});


export default Night;



/*

Then I would create an array inside the Night model storing those Dream objects.

- When loading from localStorage I would use something like JSON.parse to load a JSON array of serialized Dream objects for that Night.
- When saving: I would use JSON.stringify to save that serialized array into the localStorage for the given Night

Use Ember.js method array.pushObject(object) for adding it to array, array.removeObject(object) for removing it from the array

*/
